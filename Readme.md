# GitLab CI : Hello World
---
## Pipelines, Stages and Jobs
Pipelines are the top-level component of continuous integration, delivery, and deployment.
 - A pipeline is composed of stages. Stages are composed of jobs. 
 - All jobs in a stage perform at the same time, run in parallel.

Additionaly, note that all jobs are defined with a default (unnamed) stage unless explicitly specified. Therefore, all jobs you define will run in parallel by default. When you care about execution order (such as building before you test), then we need to consider multiple stages and job dependencies.

If all the jobs in a stage:

 - Succeed, the pipeline moves on to the next stage.
 - Fail, the next stage is not (usually) executed and the pipeline ends early.

## The Hello World Job
Let’s create our first job that simply echoes "Hello World, Here is GitLab CI".

    ``` yaml
    hello world:
      script: echo "Hello World"
	```
## Validating CI/CD YAML Configuration
GitLab comes with a linter for the YAML you write. CI Linters are especially useful to check syntax before pushing changes. This linter can be found at <project-url>/-/ci/lint. 
This can also be found by going to CI/CD -> Pipelines or CI/CD -> Jobs page and clicking the CI Lint button at the top right.

## Checking Pipeline Status

Go to the commits page of your project and see the pipeline's status for that commit.

## GitLab CI YAML
The GitLab CI configurations are specified using a YAML file called **.gitlab-ci.yml**. Here is an example:

## Good practice
